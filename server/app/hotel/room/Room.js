const mongoose = require('mongoose')

const { Schema } = mongoose

const roomSchema = new Schema({
    fullname: {type:String, required:true},
    time: {type:String, required:'enter Checkout'},
    people: {type:Number, required:'enter People'},
    kids: {type:Number, required:'enter kids'},
    roomType:{type:String, required:'enter roomType'},
    roomNumber:{type:Number, required:'enter roomNumber'},
    created_at:{type:Date, default:Date.now()}
})

const Room = mongoose.model('Room',roomSchema)
module.exports = Room
