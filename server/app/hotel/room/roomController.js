const Room = require('./Room')

module.exports = {
    create:async(req,res) =>{
        const room = new Room(req.body)
        await room.save()
        res.status(200).json(room)
    },
    get: async(req,res) =>{
        const room = await Room.find({})
        res.status(200).json(room)
    },
    remove:async(req,res)=>{
        const {roomId} = req.params
        const room = await Room.findById(roomId)

        await room.remove()

        res.status(200).json('successfull')
    },
    single: async(req,res)=>{
        const {roomId} = req.params
        const room = await Room.findById(roomId)

        res.send(room)
    }
}
