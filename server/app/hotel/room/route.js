const express = require('express')

const router = require('express-promise-router')()

const roomController = require('./roomController')

router.route('/room')
    .post(roomController.create)
    .get(roomController.get)

router.route('/room/:roomId')
    .get(roomController.single)
    .delete(roomController.remove)

// router.route('/single/:roomId')
//     .get(roomController.remove)

module.exports = router
