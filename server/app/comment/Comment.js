const mongoose = require('mongoose')

const { Schema } = mongoose


const commentSchema = new Schema({
    text:{type:String,required:true},
    name:{type:String,required:true},
    created_at: {type:Date, default:Date.now()},
    article: {type:Schema.ObjectId, ref:'Article'},
    isDeleted: {type:Boolean,default:false}
})

const Comment = mongoose.model('Comment',commentSchema)
module.exports = Comment
