const Comment = require('./Comment')
const Article = require('../article/Article')

module.exports = {
    // create:async(req,res)=>{
    //     // console.log(req.file)
    //     const { articleId } = req.params
    //     const newVideo = new Video({
    //         filename:req.file.filename,
    //         fileSize:req.file.size
    //     })
    //     // console.log(newVideo)
    //     const article = await Article.findById(articleId)
    //
    //     newVideo.article = article
    //
    //     await newVideo.save()
    //
    //     article.video.push(newVideo)
    //     await article.save()
    //
    //     res.status(200).json(newVideo)
    // }
    post:async(req,res)=>{
        const {articleId} = req.params
        const newComment = new Comment(req.body)

        const article = await Article.findById(articleId)

        newComment.article = article

        await newComment.save()

        article.comments.push(newComment)

        await article.save()

        res.status(200).json(newComment)
    },
    get: async(req,res)=>{
        const comment = await Comment.find({})

        res.status(200).json(comment)
    },
    delete: async(req,res)=>{
        const { commentId } = req.params

        const comment = await Comment.findById(commentId)
        const articleId =  comment.article

        const article = await Article.findById(articleId)

        await comment.remove()
        article.comments.pull(comment)

        await article.save()

        res.status(200).json({
            success:true,
            message: 'deleted',
            data:article
        })
    }
}
