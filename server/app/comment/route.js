const express = require('express')

const router = require('express-promise-router')()


const commentController = require('./commentController')

router.route('/comment/:articleId')
    .post(commentController.post)

router.route('/comment')
    .get(commentController.get)

router.route('/comment/:commentId')
    .get(commentController.delete)

    module.exports = router
