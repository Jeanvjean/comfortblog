const express = require('express')
const router = require('express-promise-router')()


const categoryController = require('./categoryController')

router.route('/category')
    .get(categoryController.index)
    .post(categoryController.post)


module.exports = router
