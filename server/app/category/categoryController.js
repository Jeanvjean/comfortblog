const Category = require('./Category')

module.exports = {
    index:async(req,res)=>{
        const category = await Category.find({}).populate('article')
        res.status(200).json(category)
    },
    post:async(req,res)=>{
        const category = new Category(req.body)
        await category.save()
        res.status(200).json(category)
    }
}
