const mongoose = require('mongoose')
const { Schema } = mongoose


const categorySchema = new Schema({
    name:{type:String,required:true},
    created_at:{type:Date,default:Date.now()},
    article:[{type:Schema.ObjectId,ref:'Article'}]
})

const Category = mongoose.model('Category',categorySchema)

module.exports = Category
