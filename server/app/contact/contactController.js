// const nodemailer = require('nodemailer')

module.exports = {
    send: async(req,res) =>{
         const mail = `
         <p>You have a new contact email</p>
        <h3>Details</h3>
        <ul>
        <li>Name: ${req.body.name}</li>
        <li>Email: ${req.body.email}</li>
        <li></li>
        </ul>
        <p>Message: ${req.body.message}</p>`

        console.log(mail)
            let transport = nodemailer.createTransport({
                host: '',
                port: 587,
                secure: false,
                auth: {
                    user: '',
                    pass: ''
                },
                tls:{
                    rejectUnauthorized: false
                }
            })
            // setup email data with unicode symbols
            let mailOptions = {
                from: `${req.body.email}`, // sender address
                to: 'techinfo@gmail.com', // list of receivers
                subject: 'support', // Subject line
                text: `${req.body.message}`, // plain text body
                html: `mail` // html body
            };
            transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));


            res.render('home/contact',{msg:'Email sent'})
        });
    }

}
