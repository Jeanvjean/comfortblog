const express = require('express')
const router = require('express-promise-router')()


const contactController = require('./contactController')

router.route('/contact')
    .post(contactController.send)

module.exports = router
