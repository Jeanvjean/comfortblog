const User = require('./User')
const jwt = require('jsonwebtoken')
const { jwt_SECRET } = require('./configJwt')

signToken = user =>{
    return jwt.sign({
        iss:'techmarketer.io',
        sub: user.id,
        iat: new Date().getTime(),//current time
        exp: new Date().setDate(new Date().getDate() + 1)//current time incremanted by one day
    }, jwt_SECRET)
}

module.exports = {
    signup: async(req,res)=>{
        const {email, password, adminCode} = req.value.body
        //user already exists
        const foundUser = await User.findOne({email})
        if (foundUser) {
            return res.status(403).send({error:'email already exists'})
        }
        const newUser = new User({email,password,adminCode})
        if (req.value.body.adminCode === 'tailoredAdmin') {
            newUser.isAdmin = true
        }
        console.log(newUser)
        await newUser.save()
        //generate token
        const token = signToken(newUser)

        res.status(200).json({
            newUser,
            token
        })
    },
    signin:async(req,res)=>{
        const token = signToken(req.user)
        const user = (req.user)
        // res.redirect('/')
        res.status(200).json({token,user})

    },
    signout:async(req,res)=>{
        req.logout()
        // res.redirect('/')
    },
    secret:async(req,res)=>{
        res.send('you have access to this now')
    }
}
