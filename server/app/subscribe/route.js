let express = require('express')
let router = require('express-promise-router')()

let subController = require('./subController')

    router.route('/sub')
        .post(subController.post)
 module.exports = router
