const mongoose = require('mongoose')

const { Schema } = mongoose

const videoSchema = new Schema({
    name: {type:String},
    filename: String,
    fileSize: String,
    likes: {type:Number, default:0}
    // article: {type:Schema.ObjectId, ref:'Article'}
})

module.exports = mongoose.model('Video',videoSchema)
