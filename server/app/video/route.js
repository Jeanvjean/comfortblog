const express = require('express')
const fs = require('fs')
const path = require('path')
const multer = require('multer')

const storage = multer.diskStorage({
    destination: './public/uploads/videos',
    filename: function(req,file,cb) {
        cb(null,Math.random() + '-' + file.fieldname + '-' + file.originalname)
    }
})

const upload = multer({
    storage:storage,
    fileFilter: (req,file,cb)=>{
        checkFileType(file,cb)
    }
}).single('video')

function checkFileType(file,cb) {
    const filetypes = /mp4|ogg|wma|/
    const extname = filetypes.test(path.extname(file.originalname))

    const mimetype = filetypes.test(file.mimetype)

    if (mimetype && extname) {
        return cb(null,true)
    }else {
        cb('err: not a valid format, mp4,ogg,wma acepted')
    }
}
const router = require('express-promise-router')()

const videoController = require('./videoController')

router.route('/video')
    .post(upload,videoController.create)
    .get(videoController.index)

router.route('/video/:videoId')
    .get(videoController.remove)
    .post(videoController.like)
    .delete(videoController.delete)

    module.exports = router
