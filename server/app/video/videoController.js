const Video = require('./Video')
const Article = require('../article/Article')

module.exports = {
    create:async(req,res)=> {
        const video = new Video({
            name:req.body.name,
            filename:req.file.filename,//fieldname video
            fileSize:req.file.size//fieldname video
        })
        await video.save()
        res.status(200).json(video)
    },
    index:async(req,res) => {
        const video = await Video.find({})
        res.status(200).json(video)
    },
    remove: async(req,res)=>{
        const { videoId } = req.params

        const video = await Video.findById(videoId)
        const articleId = video.article

        const article = await Article.findById(articleId)

        await video.remove()

        res.status(200).json(article)
    },
    like: async(req,res) =>{
        const action = req.body
        const {videoId} = req.params

        const counter = action === 'Like' ? 1 : 1

        const video = await Video.findById(videoId)

        // console.log(article)

        const newVideo = await video.update({$inc: {likes: counter}})

        res.status(200).json(newVideo)
    },
    delete:async(req,res) =>{
        const { videoId } = req.params

        const video = await Video.findByIdAndRemove(videoId)

        res.send({message:'video Deleted'})
    }
}
