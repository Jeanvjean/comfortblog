const Carousel = require('./Carousel')
const Article = require('../article/Article')
const Video = require('../video/Video')

module.exports = {
    post:async(req,res)=>{
        const carousel = await Carousel.create({
            title:req.body.title,
            image:req.file.filename
        })
        res.status(200).json(carousel)
    },
    // get:async(req,res)=>{
    //     const carousel = await Carousel.find({})
    //     res.send(carousel)
    // },
    likedArticles:async(req,res) =>{
        const articles = await Article.find({}).populate('category').sort({likes: -1}).limit(5)
        res.send(articles)
    },
    likedVideos:async(req,res)=>{
        const videos = await Video.find({}).sort({likes:-1}).limit(2)
        res.send(videos)
    },
    newArticle:async(req,res)=>{
        const newArticle = await Article.find({}).populate('category').sort({created_at: -1}).limit(5)
        res.send(newArticle)
    }
}
