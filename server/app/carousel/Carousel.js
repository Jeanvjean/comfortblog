const mongoose = require('mongoose')

const { Schema } = mongoose


const carouselSchema = new Schema({
    title:String,
    image:String
})

let Carousel = mongoose.model('Carousel',carouselSchema)

module.exports = Carousel
