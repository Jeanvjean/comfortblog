const express = require('express')
const router = require('express-promise-router')()
const multer = require('multer')

const storage = multer.diskStorage({
    destination: './public/uploads/carousel',
    filename: function(req,file,cb) {
        cb(null,Math.floor(Math.random()+1) + '-' + file.fieldname + '-' + file.originalname)
    }
})

const upload = multer({
    storage:storage
}).single('image')


const carouselController = require('./carouselController')

router.route('/carousel')
    .post(upload,carouselController.post)
    .get(carouselController.likedArticles)

router.route('/likedVideos')
    .get(carouselController.likedVideos)

router.route('/newArticle')
    .get(carouselController.newArticle)

module.exports = router
