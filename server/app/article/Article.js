const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')
const { Schema }   = mongoose

const articleSchema = new Schema({
    title: {type:String,required:true},
    body : {type:String,required:true},
    category:{type:Schema.ObjectId, ref:'Category'},
    created_at: {type:Date, default:Date.now()},
    image:{type:String,required:false},
    likes: {type:Number,default:0},
    comments: [{type:Schema.ObjectId, ref:'Comment'}]
})

articleSchema.plugin(mongoosePaginate)

const Article = mongoose.model('Article',articleSchema)

module.exports = Article
