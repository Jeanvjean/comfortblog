const express = require('express')
const router = require('express-promise-router')()
const passport = require('passport')
const pasConf = require('../jwtAuth/passport')
const passportJwt = passport.authenticate('jwt',{session:false})

const fs = require('fs')
const path = require('path')
const multer = require('multer')


const storage = multer.diskStorage({
    destination: '../server/public/uploads/article',
    filename: function(req,file,cb) {
        cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({
    storage:storage,
    limits:{
    fileSize: 1024 * 1024 * 20
},
fileFilter: (req,file,cb)=>{
    checkFileType(file, cb);
}
}).single('image')

function checkFileType(file,cb){
    const filetypes = /jpeg|jpg|png|JPG/
    const extname = filetypes.test(path.extname(file.originalname))

    const mimetype = filetypes.test(file.mimetype)

    if (mimetype && extname) {
        return cb(null,true)
    }else {
        cb('error: images only')
    }
}
const articleController = require('./articleController')


router.route('/article')
    .get(articleController.index)
    .post(upload,articleController.create)

router.route('/article/:articleId')
    .post(articleController.like)
    .get(articleController.singleArticle)
    .delete(articleController.delete)
    .put(articleController.update)

    module.exports = router
