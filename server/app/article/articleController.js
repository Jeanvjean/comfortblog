const Article = require('./Article')
const Category = require('../category/Category')
const pId = require('../../id/pusher')
const Pusher = require('pusher')

module.exports = {
    index: async (req,res) => {
        const article = await Article.find({}).sort({created_at: -1})
        res.status(200).json(article)
    },
    create: async(req,res) =>{
        // console.log(req.file)
        // console.log(req.body)
        const newArticle = new Article({
            body:req.body.body,
            title:req.body.title,
            category:req.body.category,
            image: req.file.filename
        })
        const category = await Category.findById(newArticle.category)

        newArticle.category = category

        await newArticle.save()


        category.article.push(newArticle)

        await category.save()

        res.status(200).json(newArticle)
    },
    like:async(req,res)=>{
        const action = req.body
        const {articleId} = req.params

        const counter = action === 'Like' ? 1 : 1

        const article = await Article.findById(articleId)

        const newArticle = await article.update({$inc: {likes: counter}})

        // let pusher = new Pusher({
        //     appId: pId.appId,
        //     key: pId.key,
        //     secret: pId.secret,
        //     cluster: 'eu',
        //     encrypted: true
        // })
        // let payload = {action,action,articleId:req.params}
        // pusher.trigger('')

        res.status(200).json(newArticle)
    },
    singleArticle: async(req,res)=>{
        const { articleId } = req.params

        const article = await Article.findById(articleId).populate('comments category')

        res.status(200).json(article)
    },
    delete:async(req,res)=>{
        const {articleId} = req.params
        const article  = await Article.findById(articleId)

        // console.log(article.category)

        const categoryId = article.category

        // console.log(categoryId)

        const category = await Category.findById(categoryId)

        await article.remove()

        category.article.pull(article)

        await category.save()

        res.send({
            message:'article deleted successfully'
        })
    },
    update:async(req,res)=>{
        const {articleId} = req.params
        const newArticle = req.body
        // console.log(newArticle)
        const result = await Article.findByIdAndUpdate(articleId,newArticle)

        res.status(200).json(result)
    }
}
