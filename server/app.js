const express = require('express')
const morgan  = require('morgan')
const cors    = require('cors')
const bodyParser = require('body-parser')
const path = require('path')
const db      = require('./db')
const mongoose= require('mongoose')
const port = process.env.PORT || 3000

// var Pusher = require('pusher')

const app = express()
const article = require('./app/article/route')
const video = require('./app/video/route')
const comment = require('./app/comment/route')
const category = require('./app/category/route')
const auth = require('./app/jwtAuth/route')
const contact = require('./app/contact/route')
const carousel = require('./app/carousel/route')
const sub = require('./app/subscribe/route')

//pusher
const pId = require('./id/pusher')

//hotel system test project
const room = require('./app/hotel/room/route')

var connection = mongoose.connect(db.staging,(err) => {
    if (err) {
        console.log('database connection error', err);
    } else {
        console.log('database connection successful');
    }
});
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({'extended':'false'}))
app.use(express.static(path.join(__dirname,'../client/dist')))
app.use(express.static(path.join(__dirname,'/public')))
app.use('/articles',express.static(path.join(__dirname,'../client/dist')))


// var pusher = new Pusher({
//   appId: pId.appId,
//   key: pId.key,
//   secret: pId.secret,
//   cluster: 'eu',
//   encrypted: true
// });

// pusher.trigger('my-channel', 'my-event', {
//   "message": "hello world"
// });
app.set('view engine', 'ejs')

app.use(cors())
app.use(morgan('dev'))

app.use(article)
app.use(video)
app.use(comment)
app.use(category)
app.use(auth)
app.use(contact)
app.use(carousel)
app.use(sub)

//hotel system test project

app.use(room)

app.listen(port,()=>{
    console.log(`server is running on ${port}`)
})

module.exports = app
