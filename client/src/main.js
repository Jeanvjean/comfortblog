// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import { sync } from 'vuex-router-sync'
import VueCkeditor from 'vue-ckeditor2'
import VueFilter from 'vue-filter'

import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VCard,
  VForm,
  VCarousel,
  VTextField,
  VParallax,
  VSelect,
  VCheckbox,
  VChip,
  VDivider,
  VMenu,
  VDialog,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  transitions,
  VPagination
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'
import Editor from '@tinymce/tinymce-vue'

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VChip,
    VDialog,
    VForm,
    VCheckbox,
    VSelect,
    VDivider,
    VCarousel,
    VTextField,
    VList,
    VCard,
    VBtn,
    VMenu,
    VIcon,
    VGrid,
    VToolbar,
    VParallax,
    transitions,
    VPagination
  },
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
    background: '#c6d0d3'
  }
})
// filters
// Vue.filter('toUppercase', (value) => {
//   return value.toUpperCase()
// })
Vue.filter('snippet', (value) => {
  return value.slice(0, 350) + '...'
})
// higlight of new articles
Vue.filter('hylight', (value) => {
  return value.slice(0, 150) + ' ...'
})

Vue.component('editor', Editor)
Vue.use(VueCkeditor)
Vue.use(require('vue-moment'))
Vue.use(VueFilter)

sync(store, router)

Vue.config.productionTip = false

/* eslint-disable */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
