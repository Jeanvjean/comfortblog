import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Article from '@/components/Article/index'
import Update from '@/components/Article/Update'
import Create from '@/components/Article/Create'
import Show from '@/components/Article/Show'
import Login from '@/components/user/Login'
import Register from '@/components/user/Register'
import Add from '@/components/video/Add'
import Video from '@/components/video/index'
import Contact from '@/components/contact/Contact'
import addCategory from '@/components/category/addCategory'

Vue.use(Router)
export default new Router({
  routes: [
      /* eslint-disable */
    {
      path: '/',
      name: 'Home',
      component: Home
  },
  {
      path:'/contact',
      name:'Contact',
      component:Contact
  },
    {
      path: '/create',
      name: 'Create',
      component: Create
  },
    {
      path: '/article/:articleId',
      name: 'Show',
      component: Show
  },
    {
      path: '/article/:articleId/update',
      name: 'Update',
      component: Update
  },
    {
      path: '/AddVideo',
      name: 'Add',
      component: Add
  },
    {
      path: '/articles',
      name: 'Article',
      component: Article
  },
    {
      path: '/addCategory',
      name: 'addCategory',
      component: addCategory
  },
  {
      path:'/videos',
      name:'Video',
      component: Video
  },
  {
      path: '/login',
      name: 'Login',
      component: Login
  },
  {
      path: '/register',
      name: 'Register',
      component: Register
  }
],
mode: 'history'
})
